﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._0.DataAccess.Generic
{
    public abstract class EntityGeneric
    {
       public DateTime registerDate { get; set; }
        public DateTime modificationDate { get; set; }
    }
}
