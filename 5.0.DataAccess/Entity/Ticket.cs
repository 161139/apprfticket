﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
namespace _5._0.DataAccess.Entity
{
    [Table("tticket")]
    public class Ticket:EntityGeneric
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string  idTicket  { get; set; }
        public string idUser {get; set;  }
        public string idService { get; set; }
        public string clientFirstName { get; set; }
        public string clientSurName { get; set; }
        public string clientEmail { get; set; }
        public string clientPhone { get; set; }
        public string description { get; set; }
        public string status { get; set; }

        [ForeignKey(nameof(idUser))]
        public User parantUser { get; set; }
        [ForeignKey(nameof(idService))]
        public Service parantService { get; set; }

        [InverseProperty("parentTicket")]
        public List<Attached> childAttached { get; set; }
    }
}
