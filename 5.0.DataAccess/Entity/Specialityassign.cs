﻿
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
namespace _5._0.DataAccess.Entity
{
    [Table("tspecialityassign")]
    public class Specialityassign: EntityGeneric

    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string  idSpecialityAssign { get; set; }
        public string idUser { get; set; }
        public string idSpeciality { get; set; }
        [ForeignKey(nameof(idUser))]
        public User parentUser { get; set; }
        [ForeignKey(nameof(idSpeciality))]
        public Speciality parentSpeciality { get; set; }
    }
}
