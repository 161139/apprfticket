﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
namespace _5._0.DataAccess.Entity
{
    [Table("tspeciality")]
    public class Speciality : EntityGeneric 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idSpeciality { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        [InverseProperty("parentSpeciality")]
        public List<Ticket> childSpecialityassign { get; set; }
    }
}
