﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
using System.Collections.Generic;

namespace _5._0.DataAccess.Entity
{   
    [Table("tuser")]
    public class User: EntityGeneric
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idUser { get; set; }
        public string firstName { get; set; }
        public string surName { get; set; }
        public string email { get; set; }
        public string password { get; set; } 
        [InverseProperty("parentUser")]
        public List<Ticket> childTicket { get; set; }
          [InverseProperty("parentUser")]
        public List<Specialityassign> childSpecialityassign { get; set; }
    }
}
