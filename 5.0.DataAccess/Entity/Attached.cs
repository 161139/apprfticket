﻿using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
namespace _5._0.DataAccess.Entity
{
    [Table("tattached")]
    class Attached:EntityGeneric
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idAttached { get; set; }
        public string idTicket { get; set; }
        public string name { get; set; }
        public string extension { get; set; }
        [ForeignKey(nameof(idTicket))]
        public Ticket parentTicket { get; set; }
    }
}
