﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using _5._0.DataAccess.Generic;
using System.Collections.Generic;

namespace _5._0.DataAccess.Entity
{
    [Table("tservice")]
    public class Service : EntityGeneric
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idService { get; set; }
        public string name { get; set; }
        [InverseProperty("parentService")]
        public List<Ticket> childTicket { get; set; }
    }  
}
