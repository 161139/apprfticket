﻿
using System.ComponentModel.DataAnnotations;
using _0._0.DataTransferLayer.Generic;
namespace _0._0.DataTransferLayer.Dto
{
    public class DtoUser:DtoGeneric
    {
        public string idUser { get; set; }
        [Required(ErrorMessage="el campo firname es requerido.")]
        public string firstName { get; set; }
        [Required(ErrorMessage = "el campo surname es requerido.")]
        public string surName { get; set; }
        [Required(ErrorMessage = "el campo email es requerido.")]
        public string email { get; set; }
        [Required(ErrorMessage = "el campo password es requerido.")]
        public string password { get; set; }
    }
}
